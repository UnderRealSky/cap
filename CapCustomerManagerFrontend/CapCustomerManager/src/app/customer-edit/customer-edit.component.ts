import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { CoreapiService } from "../coreapi.service";
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
} from "@angular/forms";

@Component({
  selector: "app-customer-edit",
  templateUrl: "./customer-edit.component.html",
  styleUrls: ["./customer-edit.component.scss"]
})
export class CustomerEditComponent implements OnInit {
  customerForm: FormGroup;
  customerId: string = "";
  name: string = "";
  email: string = "";
  phoneNumber: string = "";
  city: string = "";
  country: string = "";
  dateOfBirth: Date = null;
  isActive: boolean;
  isLoadingResults = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: CoreapiService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getCustomer(this.route.snapshot.params["id"]);
    this.customerForm = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, Validators.required],
      city: [null, Validators.required],
      country: [null, Validators.required],
      phoneNumber: [null],
      dateOfBirth: [null],
      isActive: [null]
    });
  }
  getCustomer(id: string) {
    this.api.getCustomer(id).subscribe(data => {
      console.log(data);
      console.log("data");
      this.customerId = data.customerId;
      this.customerForm.setValue({
        name: data.name,
        city: data.country,
        country: data.country,
        dateOfBirth: data.dateOfBirth,
        isActive: data.isActive,
        email: data.email,
        phoneNumber: data.phoneNumber
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateCustomer(this.customerId, form).subscribe(
      resource => {
        let id = resource["id"];
        this.isLoadingResults = false;
        this.router.navigate(["/custmer", id]);
      },
      err => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
  }

  ShowCustomerDetails() {
    this.router.navigate(["/customer", this.customerId]);
  }
}
