import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { catchError, tap, map } from "rxjs/operators";
import { Customer } from "./customer";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
const apiUrl = "http://localhost:53974/api/Customer";

@Injectable({
  providedIn: "root"
})
export class CoreapiService {
  constructor(private http: HttpClient) {}

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /// API methods
  getCustomers(): Observable<any[]> {
    return this.http.get<Customer[]>(apiUrl).pipe(
      tap(customers => console.log("Fetch customers")),
      catchError(this.handleError("getCustomers", []))
    );
  }

  getCustomer(id: string): Observable<Customer> {
    const url = `${apiUrl}/${id}`;

    return this.http.get<Customer>(url).pipe(
      tap(_ => console.log(`fetched customer id=${id}`)),
      catchError(this.handleError<Customer>(`getCustomer id=${id}`))
    );
  }

  addCustomer(customer): Observable<Customer> {
    return this.http.post<Customer>(apiUrl, customer, httpOptions).pipe(
      tap(customer =>
        console.log(`added customer w/ id=${customer.customerId}`)
      ),
      catchError(this.handleError<Customer>("addCustomer"))
    );
  }

  updateCustomer(id: string, customer): Observable<any> {
    const url = `${apiUrl}/${id}`;
    console.log("put");
    console.log(customer);
    return this.http.put(url, customer, httpOptions).pipe(
      tap(_ => console.log(`updated customer id=${id}`)),
      catchError(this.handleError<any>("updateCustomer"))
    );
  }

  deleteCustomer(id: string): Observable<Customer> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Customer>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted customer id=${id}`)),
      catchError(this.handleError<Customer>("deleteCustomer"))
    );
  }
}
