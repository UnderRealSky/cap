import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CoreapiService } from "../coreapi.service";
import { Customer } from "../customer";
import { EmptyTextCover } from "../extentions/pipes/emptyTextcover.pipe";

@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.scss"]
})
export class CustomerComponent implements OnInit {
  customer: Customer = {
    customerId: "",
    name: "",
    email: "",
    city: null,
    country: null,
    dateOfBirth: null,
    isActive: false,
    phoneNumber: ""
  };
  isLoadingResults = true;

  constructor(
    private route: ActivatedRoute,
    private api: CoreapiService,
    private router: Router
  ) {}

  ngOnInit() {
    console.log("parametr Id:" + this.route.snapshot.params["id"]);
    this.getDetails(this.route.snapshot.params["id"]);
  }

  getDetails(id: string) {
    this.api.getCustomer(id).subscribe(data => {
      this.customer = data;
      console.log("pobrany klient");
      console.log(this.customer);
      this.isLoadingResults = false;
    });
  }

  deleteCustomer(id: string) {
    this.isLoadingResults = true;
    this.api.deleteCustomer(id).subscribe(
      res => {
        this.isLoadingResults = false;
        this.router.navigate(["/customers"]);
      },
      err => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
  }
}
