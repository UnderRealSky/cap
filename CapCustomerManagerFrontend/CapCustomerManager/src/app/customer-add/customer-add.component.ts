import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CoreapiService } from "../coreapi.service";
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
} from "@angular/forms";

@Component({
  selector: "app-customer-add",
  templateUrl: "./customer-add.component.html",
  styleUrls: ["./customer-add.component.scss"]
})
export class CustomerAddComponent implements OnInit {
  customerForm: FormGroup;
  customerId: string = "";
  name: string = "";
  email: string = "";
  phoneNumber: string = "";
  city: string = "";
  country: string = "";
  dateOfBirth: Date = new Date(0, 0, 0);
  isActive: boolean = false;
  isLoadingResults = false;

  constructor(
    private router: Router,
    private api: CoreapiService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.customerForm = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, Validators.required],
      city: [null, Validators.required],
      country: [null, Validators.required],
      phoneNumber: [null],
      dateOfBirth: [null],
      isActive: [null]
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;

    this.api.addCustomer(form).subscribe(
      customer => {
        console.log("added new cusoomer");
        this.router.navigate(["/customers"]);
      }
      //// not working :(
      // res => {
      //   //let id = res["customerId"];
      //   // this.isLoadingResults = false;
      //   //console.log("wynik: " + id);
      //   //this.router.navigate(["/customers", id]);
      // },
      // err => {
      //   console.log(err);
      //   this.isLoadingResults = false;
      // }
    );
  }
}
