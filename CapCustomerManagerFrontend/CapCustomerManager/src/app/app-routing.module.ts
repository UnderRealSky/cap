import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CustomerComponent } from "./customer/customer.component";
import { CustomerAddComponent } from "./customer-add/customer-add.component";
import { CustomerEditComponent } from "./customer-edit/customer-edit.component";
import { CustomersOverwievComponent } from "./customers-overwiev/customers-overwiev.component";

const routes: Routes = [
  {
    path: "customers",
    component: CustomersOverwievComponent,
    data: { title: "List of Customers" }
  },
  {
    path: "customer/:id",
    component: CustomerComponent,
    data: { title: "Customer Details" }
  },
  {
    path: "customer-add",
    component: CustomerAddComponent,
    data: { title: "Add Customer" }
  },
  {
    path: "customer-edit/:id",
    component: CustomerEditComponent,
    data: { title: "Edit Customer" }
  },
  {
    path: "",
    redirectTo: "customers",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
