export class Customer {
  customerId: string;
  name: string;
  dateOfBirth: Date;
  city: string;
  country: string;
  phoneNumber: string;
  email: string;
  isActive: boolean;
}
