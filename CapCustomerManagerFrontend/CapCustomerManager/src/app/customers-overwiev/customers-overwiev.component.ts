import { Component, OnInit } from "@angular/core";
import { CoreapiService } from "../coreapi.service";
import { Customer } from "../customer";

@Component({
  selector: "app-customers-overwiev",
  templateUrl: "./customers-overwiev.component.html",
  styleUrls: ["./customers-overwiev.component.scss"]
})
export class CustomersOverwievComponent implements OnInit {
  displayedColumns: string[] = ["name", "city", "email"];
  data: Customer[] = [];
  isLoadingResults = true;

  constructor(private api: CoreapiService) {}

  ngOnInit() {
    this.api.getCustomers().subscribe(
      res => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      },
      err => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
    let test = this.api.getCustomers();
  }
}
