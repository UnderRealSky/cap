import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersOverwievComponent } from './customers-overwiev.component';

describe('CustomersOverwievComponent', () => {
  let component: CustomersOverwievComponent;
  let fixture: ComponentFixture<CustomersOverwievComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersOverwievComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersOverwievComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
