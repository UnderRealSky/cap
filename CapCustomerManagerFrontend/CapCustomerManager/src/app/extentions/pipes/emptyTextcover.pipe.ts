import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
  name: "emptyTextCover"
})
export class EmptyTextCover implements PipeTransform {
  transform(val: string): string {
    if (val === "" || val === undefined || val === null) {
      return "no data provided";
    } else {
      return val;
    }
  }
}
