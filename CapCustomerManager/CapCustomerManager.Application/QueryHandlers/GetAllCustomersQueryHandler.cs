﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapCustomerManager.Definitions.Qeries;
using CapCustomerManager.Definitions.ResponseDTOs;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using CapCustomerManager.Domain.Interfaces.Handlers;

namespace CapCustomerManager.Application.QueryHandlers
{
    public class GetAllCustomersQueryHandler : IQueryHandler<GetAllCustomersquery, List<CapCustomerResponseDto>>
    {
        private readonly ICustomerRepository<CapCustomer> _repository;

        public GetAllCustomersQueryHandler(ICustomerRepository<CapCustomer> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<List<CapCustomerResponseDto>> Execute(GetAllCustomersquery query)
        {
            var results =  await _repository.GetAllCustomersAsync();
            var parsedResults = results.Select(ConvertDomainToDto).ToList();
            return parsedResults;
        }

        private CapCustomerResponseDto ConvertDomainToDto(CapCustomer customer)
        {
            return new CapCustomerResponseDto()
            {
                CustomerId = customer.CustomerId,
                Name = customer.Name,
                Email = customer.Email,
                City = customer.City,
                Country = customer.Country,
                DateOfBirth = customer.DateOfBirth,
                IsActive = customer.IsActive,
                PhoneNumber = customer.PhoneNumber
            };
        }

    }
}