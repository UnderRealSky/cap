﻿using System;
using System.Threading.Tasks;
using CapCustomerManager.Definitions.Qeries;
using CapCustomerManager.Definitions.ResponseDTOs;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using CapCustomerManager.Domain.Interfaces.Handlers;

namespace CapCustomerManager.Application.QueryHandlers
{
    public class GetCustomerQueryHandler: IQueryHandler<GetCustomerByIdQuery, CapCustomerResponseDto>
    {
        private readonly ICustomerRepository<CapCustomer> _repository;

        public GetCustomerQueryHandler(ICustomerRepository<CapCustomer> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<CapCustomerResponseDto> Execute(GetCustomerByIdQuery query)
        {
            var results = await _repository.GetCustomer(query.Identification.Id);
            return new CapCustomerResponseDto()
            {
                CustomerId = results.CustomerId,
                City = results.City,
                IsActive = results.IsActive,
                DateOfBirth = results.DateOfBirth,
                Email = results.Email,
                Country = results.Country,
                Name = results.Name,
                PhoneNumber = results.PhoneNumber
            };
        }
    }
}