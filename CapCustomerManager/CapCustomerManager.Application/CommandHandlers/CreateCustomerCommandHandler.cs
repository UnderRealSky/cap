﻿using System;
using System.Threading.Tasks;
using CapCustomerManager.Definitions.Commands;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using CapCustomerManager.Domain.Interfaces.Handlers;

namespace CapCustomerManager.Application.CommandHandlers
{
    public class CreateCustomerCommandHandler : ICommandHandler<CreateCustomerCommand>
    {
        private readonly ICustomerRepository<CapCustomer> _repository;

        public CreateCustomerCommandHandler(ICustomerRepository<CapCustomer> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task Execute(CreateCustomerCommand command)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));


            try
            {
                var newCustomer = ConvertToCapCustomer(command);

                var creationResult = await _repository.CreateNewCustomer(newCustomer);
                if(creationResult != newCustomer.CustomerId)
                    throw new Exception("Could not create new customer");
            }
            catch (Exception exception)
            {
                throw new Exception($"Creating a new Customer failed: {exception.Message}", exception);
            }
        }

        private static CapCustomer ConvertToCapCustomer(CreateCustomerCommand command)
        {
            //TODO: Add converter or mapper class
            var newCustomer = new CapCustomer()
            {
                CustomerId = command.Id,
                Name = command.Name,
                Email = command.Email,
                City = command.City,
                Country = command.Country,
                DateOfBirth = command.DateOfBirth,
                PhoneNumber = command.PhoneNumber,
                IsActive = command.IsActive
            };
            return newCustomer;
        }
    }
}