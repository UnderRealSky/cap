﻿using System;
using System.Threading.Tasks;
using CapCustomerManager.Definitions.Commands;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using CapCustomerManager.Domain.Interfaces.Handlers;

namespace CapCustomerManager.Application.CommandHandlers
{
    public class UpdateCustomerCommandHandler  : ICommandHandler<UpdateCustomerCommand>
    {
        private readonly ICustomerRepository<CapCustomer> _repository;

        public UpdateCustomerCommandHandler(ICustomerRepository<CapCustomer> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task Execute(UpdateCustomerCommand command)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));


            try
            {
                var newCustomer = ConvertToCapCustomer(command);

                await _repository.UpdateCustomer(command.CustomerId, newCustomer);
                
            }
            catch (Exception exception)
            {
                throw new Exception($"Updating a customer failed: {exception.Message}", exception);
            }
        }

        private static CapCustomer ConvertToCapCustomer(UpdateCustomerCommand command)
        {
            //TODO: Add converter or mapper class
            var newCustomer = new CapCustomer()
            {
                CustomerId = command.CustomerId,
                Name = command.Name,
                Email = command.Email,
                City = command.City,
                Country = command.Country,
                DateOfBirth = command.DateOfBirth,
                PhoneNumber = command.PhoneNumber,
                IsActive = command.IsActive
            };
            return newCustomer;
        }
    }
}