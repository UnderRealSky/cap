﻿using System.Threading.Tasks;
using CapCustomerManager.Definitions.Commands;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using CapCustomerManager.Domain.Interfaces.Handlers;

namespace CapCustomerManager.Application.CommandHandlers
{
    public class DeleteCustomerCommandHandler : ICommandHandler<DeleteCustomerCommand>
    {
        private readonly ICustomerRepository<CapCustomer> _repository;

        public DeleteCustomerCommandHandler(ICustomerRepository<CapCustomer> repository)
        {
            _repository = repository;
        }

        public async Task Execute(DeleteCustomerCommand command)
        {
            await _repository.DeteleCustomer(command.CustomerId);
        }
    }
}