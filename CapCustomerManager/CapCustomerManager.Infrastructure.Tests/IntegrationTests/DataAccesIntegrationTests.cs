﻿using System;
using CapCustomerManager.DataAccess.Repository;
using CapCustomerManager.DataAccess.Repository.Context;
using CapCustomerManager.Domain.Core;
using FluentAssertions;
using Xunit;

namespace CapCustomerManager.Infrastructure.Tests.IntegrationTests
{
    public class DataAccesIntegrationTests
    {
        [Fact]
        public async void  CreatingATestCustomerSavesToDatabase(

        )
        {
            // a
            var testingCustomerId = Guid.NewGuid();
            var dummyCustomer = NSubstitute.Substitute.For<CapCustomer>();
            dummyCustomer.Name = "TomTest";
            dummyCustomer.CustomerId = testingCustomerId;
            
            var updatedDummyCustomer = dummyCustomer;
            updatedDummyCustomer.Country = "Poland";
            var sut = new CustomerRepository(new CapCustomerManagerDbContextFactory());

            // aa
            //Action actionCreate = async () => { var x = await sut.CreateNewCustomer(dummyCustomer); };
            await sut.CreateNewCustomer(dummyCustomer); 
            //Action actionUpdate = async () => { sut.UpdateCustomer(testingCustomerId, updatedDummyCustomer); };
            await sut.UpdateCustomer(testingCustomerId, updatedDummyCustomer);
           // Action actionDelete = () => { sut.DeteleCustomer(testingCustomerId); };
            await sut.DeteleCustomer(testingCustomerId);

            // aaa

        }


        [Fact]
        public async void Greate_Get_Delete_Customer(

        )
        {
            // a
            var dummyCustomer = NSubstitute.Substitute.For<CapCustomer>();
            var testingCustomerId = Guid.NewGuid();
            var testingNameId = DateTime.Now.Ticks.ToString().Substring(7);
            dummyCustomer.Name = $"TomTest{testingNameId}";
            dummyCustomer.CustomerId = testingCustomerId;

            var sut = new CustomerRepository(new CapCustomerManagerDbContextFactory());

            // aa
            
            await sut.CreateNewCustomer(dummyCustomer);
            
            var getResults = await sut.GetCustomer(testingCustomerId);
            
            // aaa
            getResults.CustomerId.Should().Be(testingCustomerId);
            getResults.Name.Should().Contain(testingNameId);

            await sut.DeteleCustomer(testingCustomerId);


        }
    }
}