﻿using System;
using CapCustomerManager.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace CapCustomerManager.DataAccess.Repository.Context
{
    public class CapCustomerManagerDbContext : DbContext
    {


        public DbSet<CustomerEntity> CustomerEntity { get; set; }

        public CapCustomerManagerDbContext()
        {
            var connectionString = this.Database.GetDbConnection().ConnectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerEntity>()
                .HasKey(c => c.CustomerId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CapCustomerManager.SqlContext"].ConnectionString;

            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}