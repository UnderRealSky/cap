﻿using Microsoft.EntityFrameworkCore;

namespace CapCustomerManager.DataAccess.Repository.Context
{
    public interface IProvideContext<out TContext> where TContext: DbContext
    {
        TContext Obtain();
    }
}