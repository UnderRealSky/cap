﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CapCustomerManager.DataAccess.Repository.Context
{
    public class CapCustomerManagerDbContextFactory : IProvideContext<CapCustomerManagerDbContext>
    {
        
        public CapCustomerManagerDbContext Obtain()
        {
            return new CapCustomerManagerDbContext();
        }
    }
}