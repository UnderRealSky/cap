﻿using CapCustomerManager.DataAccess.Repository.Context;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CapCustomerManager.DataAccess.Repository
{
    public static class DataAccessProfile
    {
        public static IServiceCollection ResolveDataAccessDiProfile(this IServiceCollection services)
        {
            services.AddTransient<IProvideContext<CapCustomerManagerDbContext>, CapCustomerManagerDbContextFactory>();
            services.AddTransient<ICustomerRepository<CapCustomer>, CustomerRepository>();
            return services;
        }
    }
}