﻿using CapCustomerManager.DataAccess.Entities;
using CapCustomerManager.DataAccess.Repository.Context;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapCustomerManager.DataAccess.Repository
{
    public class CustomerRepository : ICustomerRepository<CapCustomer>
    {
        private readonly IProvideContext<CapCustomerManagerDbContext> _contextFactory;

        public CustomerRepository(IProvideContext<CapCustomerManagerDbContext> contextFactory)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
        }

        public async Task<Guid> CreateNewCustomer(CapCustomer customer)
        {
            try
            {
                var context = new CapCustomerManagerDbContext();
                CustomerEntity customerEntity = ConvertDomainToEntity(customer);
                var created = context.CustomerEntity.Add(customerEntity);
                await context.SaveChangesAsync();
                return customerEntity.CustomerId;
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Cannot Save new customer because: {exception.Message}");
                throw;
            }
        }

        public async Task DeteleCustomer(Guid customerId)
        {
            var context = new CapCustomerManagerDbContext();
            var entityToRemove = context.CustomerEntity.FirstOrDefault(c => c.CustomerId == customerId);
            if (entityToRemove != null)
            {
                context.CustomerEntity.Remove(entityToRemove);
                await context.SaveChangesAsync();
            }
            else
                throw new Exception("Cannot find item to remove");
        }

        public async Task<List<CapCustomer>> GetAllCustomersAsync()
        {
            var context = _contextFactory.Obtain();
            var results = await context.CustomerEntity.ToListAsync();
            var output = results.Select(ConvertEntityToDomain).ToList();
            return output;
        }

        public async Task<CapCustomer> GetCustomer(Guid customerId)
        {
            var context = _contextFactory.Obtain();
            var results = await context.CustomerEntity.FirstOrDefaultAsync(c => c.CustomerId == customerId);
            if (results != null)
                return ConvertEntityToDomain(results);
            else
            {
                throw new Exception("Customer not found in database");
            }
        }

        public async Task UpdateCustomer(Guid customerId, CapCustomer updatedData)
        {
            var context = new CapCustomerManagerDbContext();

            var entityToUpdate = await context.CustomerEntity.AnyAsync(c => c.CustomerId == customerId);
            if (entityToUpdate)
            {
                context.CustomerEntity.Update(ConvertDomainToEntity(updatedData));
                await context.SaveChangesAsync();
            }
        }

        private static CustomerEntity ConvertDomainToEntity(CapCustomer customer)
        {
            return new CustomerEntity()
            {
                CustomerId = customer.CustomerId,
                Name = customer.Name,
                Email = customer.Email,
                City = customer.City,
                Country = customer.Country,
                DateOfBirth = customer.DateOfBirth,
                IsActive = customer.IsActive,
                PhoneNumber = customer.PhoneNumber
            };
        }

        private static CapCustomer ConvertEntityToDomain(CustomerEntity customer)
        {
            return new CapCustomer()
            {
                CustomerId = customer.CustomerId,
                Name = customer.Name,
                Email = customer.Email,
                City = customer.City,
                Country = customer.Country,
                DateOfBirth = customer.DateOfBirth,
                IsActive = customer.IsActive,
                PhoneNumber = customer.PhoneNumber
            };
        }
    }
}