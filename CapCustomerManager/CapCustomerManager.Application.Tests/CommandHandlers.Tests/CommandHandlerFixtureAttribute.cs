﻿using AutoFixture;
using AutoFixture.Xunit2;


using System;
using AutoFixture.AutoNSubstitute;

namespace CapCustomerManager.Application.Tests.CommandHandlers.Tests
{
    public class CommandHandlerFixtureAttribute : AutoDataAttribute
    {
        public CommandHandlerFixtureAttribute() : base(FixtureFactory)
        {


        }

        private static IFixture FixtureFactory()
        {
            var fixture = new Fixture();
            fixture.Customize(new XunitCustomisation());
            return fixture;
        }
    }


    public class XunitCustomisation : ICustomization
    {
        public void Customize(IFixture fixture)
        {

            fixture.Customize(new AutoNSubstituteCustomization());
        }
    }
}
