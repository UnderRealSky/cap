﻿using System;
using AutoFixture.Xunit2;
using CapCustomerManager.Application.CommandHandlers;
using CapCustomerManager.Definitions.Commands;
using CapCustomerManager.Domain.Core;
using CapCustomerManager.Domain.Interfaces;
using CapCustomerManager.Domain.Interfaces.Handlers;
using FluentAssertions;
using Xunit;

namespace CapCustomerManager.Application.Tests.CommandHandlers.Tests
{
    public class CreateCustomerCommandHandlerTest
    {
        [Theory, CommandHandlerFixtureAttribute]
        public void GivenCommandHandler_WhenNullCommandPassed_ThenThrowsException(
            ICustomerRepository<CapCustomer> repository
            )
        {
            // a
            ICommandHandler<CreateCustomerCommand> sut = new CreateCustomerCommandHandler(repository);
            CreateCustomerCommand nullCommand = null;
            // aa
            Action action = () => { sut.Execute(nullCommand); };
            // aaa
            action.Should().Throw<ArgumentNullException>();
        }
    }
}
