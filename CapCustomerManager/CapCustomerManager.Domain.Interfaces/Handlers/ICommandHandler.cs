﻿using System.Threading.Tasks;

namespace CapCustomerManager.Domain.Interfaces.Handlers
{
    public interface ICommandHandler<in TCommand>
    {
        Task Execute(TCommand command);
    }
}