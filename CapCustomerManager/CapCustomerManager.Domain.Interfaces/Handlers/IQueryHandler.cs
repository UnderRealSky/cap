﻿using System.Threading.Tasks;

namespace CapCustomerManager.Domain.Interfaces.Handlers
{
    public interface IQueryHandler<in TQuery, TResponse> where TResponse: new()
    {
        Task<TResponse> Execute(TQuery query);
    }
}