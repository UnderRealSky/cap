﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapCustomerManager.Domain.Interfaces
{
    public interface ICustomerRepository<T>
    {
        Task<Guid> CreateNewCustomer(T customer);
        Task<List<T>> GetAllCustomersAsync();
        Task<T> GetCustomer(Guid customerId);
        Task UpdateCustomer(Guid customerId, T updatedData);
        Task DeteleCustomer(Guid customerId);

    }
}