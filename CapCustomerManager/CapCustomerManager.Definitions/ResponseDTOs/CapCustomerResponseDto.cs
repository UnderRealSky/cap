﻿using System;

namespace CapCustomerManager.Definitions.ResponseDTOs
{
    public class CapCustomerResponseDto
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
    }
}