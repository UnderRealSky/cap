﻿using System;

namespace CapCustomerManager.Definitions.Commands
{
    public class DeleteCustomerCommand
    {
        public Guid CustomerId { get; set; }
    }
}