﻿using System;

namespace CapCustomerManager.Definitions.Commands
{
    public class UpdateCustomerCommand
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

    }
}