﻿using CapCustomerManager.Definitions.Commands;
using CapCustomerManager.Definitions.Qeries;
using CapCustomerManager.Definitions.RequestDTOs;
using CapCustomerManager.Definitions.RequestsDTOs;
using CapCustomerManager.Definitions.ResponseDTOs;
using CapCustomerManager.Domain.Interfaces.Handlers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapCustomerManager.Infrastructure.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICommandHandler<CreateCustomerCommand> _createCustomercommandHandler;
        private readonly ICommandHandler<UpdateCustomerCommand> _updateCommandHandler;
        private readonly ICommandHandler<DeleteCustomerCommand> _deleteCommandHandler;
        private readonly IQueryHandler<GetCustomerByIdQuery, CapCustomerResponseDto> _getCustomerByIdQueryHandlerHandler;
        private readonly IQueryHandler<GetAllCustomersquery, List<CapCustomerResponseDto>> _getCustomersQueryHandlerHandler;


        public CustomerController(ICommandHandler<CreateCustomerCommand> createCustomercommandHandler,
            ICommandHandler<UpdateCustomerCommand> updateCommandHandler,
            ICommandHandler<DeleteCustomerCommand> deleteCommandHandler,
            IQueryHandler<GetCustomerByIdQuery, CapCustomerResponseDto> getCustomerByIdQueryHandlerHandler, 
            IQueryHandler<GetAllCustomersquery, List<CapCustomerResponseDto>> getCustomersQueryHandlerHandler)
        {
            _createCustomercommandHandler = createCustomercommandHandler ?? throw new ArgumentNullException(nameof(createCustomercommandHandler));
            _updateCommandHandler = updateCommandHandler ?? throw new ArgumentNullException(nameof(updateCommandHandler));
            _deleteCommandHandler = deleteCommandHandler ?? throw new ArgumentNullException(nameof(deleteCommandHandler));
            _getCustomerByIdQueryHandlerHandler = getCustomerByIdQueryHandlerHandler ?? throw new ArgumentNullException(nameof(getCustomerByIdQueryHandlerHandler));
            _getCustomersQueryHandlerHandler = getCustomersQueryHandlerHandler ?? throw new ArgumentNullException(nameof(getCustomersQueryHandlerHandler));
        }

        [HttpGet("{id}", Name = "GetCapCustomer")]
        [ProducesResponseType(200, Type = typeof(IActionResult))]
        [ProducesResponseType(404)]
        [ProducesDefaultResponseType(typeof(CapCustomerResponseDto))]
        public ActionResult<CapCustomerResponseDto> GetCustomer(string id)
        {
            if (Guid.TryParse(id, out Guid requestedcustomerId))
            {
                var query = new GetCustomerByIdQuery() { Identification = new CustomerIdentyfier() { Id = requestedcustomerId } };
                var results = _getCustomerByIdQueryHandlerHandler.Execute(query);
                if (results != null)
                    return Ok(results.Result);
                return NotFound($"Provided Id: {id} does not exist");
            }
            else
            {
                //throw new ArgumentException($"Provided Id: {customerId} is in wrong format");
                return BadRequest($"Provided Id: {id} is in wrong format");
            }
        }

        [HttpGet(Name = "GetAllCapCustomers")]
        [ProducesResponseType(200, Type = typeof(IActionResult))]
        [ProducesResponseType(404)]
        [ProducesDefaultResponseType(typeof(CapCustomerResponseDto))]
        public ActionResult<CapCustomerResponseDto> GetAllCustomers()
        {
            
                var query = new GetAllCustomersquery();
                var results = _getCustomersQueryHandlerHandler.Execute(query);

                if (results != null)
                    return Ok(results.Result);
            return BadRequest();

        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(IActionResult))]
        public async Task<ActionResult<CapCustomerResponseDto>> CreateCustomer([FromBody] CreateCustomerRequestDto request)
        {
            try
            {
                var command = new CreateCustomerCommand()
                {
                    Id = Guid.NewGuid(),
                    Name = request.Name,
                    Email = request.Email,
                    PhoneNumber = request.PhoneNumber,
                    City = request.City,
                    Country = request.Country,
                    DateOfBirth = request.DateOfBirth,
                    IsActive = request.IsActive
                };

                await _createCustomercommandHandler.Execute(command);


                return CreatedAtRoute("GetCapCustomer", new { id = command.Id.ToString() }, command);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(IActionResult))]
        public async Task<ActionResult<CapCustomerResponseDto>> UpdateCustomer(string id, [FromBody] UpdateCustomerRequestDto request)
        {
            if (Guid.TryParse(id, out Guid requestedcustomerId))
            {
                var command = new UpdateCustomerCommand()
                {
                    CustomerId = requestedcustomerId,
                    Name = request.Name,
                    Email = request.Email,
                    PhoneNumber = request.PhoneNumber,
                    City = request.City,
                    Country = request.Country,
                    DateOfBirth = request.DateOfBirth,
                    IsActive = request.IsActive
                };

                await _updateCommandHandler.Execute(command);

                return CreatedAtRoute("GetCapCustomer", new {id = command.CustomerId.ToString()}, command);
            }
            return BadRequest($"Unable to update customer with id: {id}");
        }

        [HttpDelete("{id}", Name = "DeleteCustomer")]
        //oute("{CustomerId}")]
        [ProducesResponseType(200, Type = typeof(IActionResult))]
        public async Task<ActionResult<CapCustomerResponseDto>> DeleteCustomer(string id)
        {
            try
            {
                if (Guid.TryParse(id, out Guid customerId))
                    await _deleteCommandHandler.Execute(new DeleteCustomerCommand() { CustomerId = customerId });

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}