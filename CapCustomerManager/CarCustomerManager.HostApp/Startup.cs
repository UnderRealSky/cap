﻿using CapCustomerManager.DataAccess.Repository;
using CarCustomerManager.HostApp.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Reflection;

//using TodoApi.Models;

namespace CarCustomerManager.HostApp
{
    /// <summary>
    ///
    /// </summary>
    public class Startup
    {/// <summary>
     ///
     /// </summary>
     /// <param name="services"></param>

        #region snippet_ConfigureServices

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Capgemini Customer Manager API",
                    Description = "a module which will be responsible for creating, removing and updating data of Customer with the following fields: managing customer data.",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "grzegorz busek",
                        Email = "gbusek@gmail.com",
                        Url = string.Empty
                    },
                });
                //c.DocumentFilter<TestFilter>();
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // Register dependencies
            services.ResolveDataAccessDiProfile();
            services.ResolveQueryHandlingProfile();
            services.ResolveCommandHandlinProfile();
        }

        #endregion snippet_ConfigureServices

        #region snippet_Configure

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();
            //app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvc(
                //routes =>
                //{
                //    routes.MapRoute(
                //        name: "GetCustomer",
                //        template: "GetCustomer/{customerId}",
                //        defaults: new {controller = "Customer"}
                //    );
                //}

                );
        }

        #endregion snippet_Configure

        public class TestFilter : IDocumentFilter
        {
            public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
            {
                swaggerDoc.Schemes = new string[] { "https" };
            }
        }
    }
}