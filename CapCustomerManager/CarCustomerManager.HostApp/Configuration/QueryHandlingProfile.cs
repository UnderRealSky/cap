﻿using System.Collections.Generic;
using CapCustomerManager.Application.QueryHandlers;
using CapCustomerManager.Definitions.Qeries;
using CapCustomerManager.Definitions.ResponseDTOs;
using CapCustomerManager.Domain.Interfaces.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace CarCustomerManager.HostApp.Configuration
{
    public static class QueryHandlingProfile
    {
        public static IServiceCollection ResolveQueryHandlingProfile(this IServiceCollection services)
        {
            services.AddTransient<IQueryHandler<GetCustomerByIdQuery, CapCustomerResponseDto>, GetCustomerQueryHandler>();
            services.AddTransient<IQueryHandler<GetAllCustomersquery, List<CapCustomerResponseDto>>,GetAllCustomersQueryHandler>();
            return services;
        }
    }
}