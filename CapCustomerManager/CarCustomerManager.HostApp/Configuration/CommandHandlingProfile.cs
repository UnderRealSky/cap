﻿using CapCustomerManager.Application.CommandHandlers;
using CapCustomerManager.Definitions.Commands;
using CapCustomerManager.Domain.Interfaces.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace CarCustomerManager.HostApp.Configuration
{
    public static class CommandHandlingProfile
    {
        public static IServiceCollection ResolveCommandHandlinProfile(this IServiceCollection services)
        {
            services.AddTransient<ICommandHandler<CreateCustomerCommand>, CreateCustomerCommandHandler>();
            services.AddTransient<ICommandHandler<UpdateCustomerCommand>, UpdateCustomerCommandHandler>();
            services.AddTransient<ICommandHandler<DeleteCustomerCommand>, DeleteCustomerCommandHandler>();
            return services;
        }
    }
}